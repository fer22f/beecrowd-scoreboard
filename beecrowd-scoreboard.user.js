// ==UserScript==
// @name         Placar do BeeCrowd
// @namespace    http://tampermonkey.net/
// @version      2024-05-24.alpha.12
// @description  Coloca mil funcionalidades novas no Placar do BeeCrowd
// @author       Fernando K
// @match        https://judge.beecrowd.com/pt/users/contest/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=beecrowd.com
// @grant        none
// @run-at       document-start
// @license      GPL-3.0-or-later; https://www.gnu.org/licenses/gpl-3.0.txt
// ==/UserScript==

(async function() {
  'use strict';

  const stateToRegion = new Map(Object.entries({ "AM": "Norte", "RR": "Norte", "AP": "Norte", "PA": "Norte", "TO": "Norte", "RO": "Norte", "AC": "Norte", "MA": "Nordeste", "PI": "Nordeste", "CE": "Nordeste", "RN": "Nordeste", "PE": "Nordeste", "PB": "Nordeste", "SE": "Nordeste", "AL": "Nordeste", "BA": "Nordeste", "MT": "Centro-Oeste", "MS": "Centro-Oeste", "GO": "Centro-Oeste", "DF": "Centro-Oeste", "SP": "Sudeste", "RJ": "Sudeste", "ES": "Sudeste", "MG": "Sudeste", "PR": "Sul", "RS": "Sul", "SC": "Sul" }));
  const institutionInfo = new Map(Object.entries({ "UFF": { "state": "RJ" }, "UFMA": { "state": "MA" }, "UFMG": { "state": "MG" }, "UFMS": { "state": "MS" }, "UFRGS": { "state": "RS" }, "UFRJ": { "state": "RJ" }, "UFRN": { "state": "RN" }, "UFRRJ": { "state": "RJ" }, "UFPA": { "state": "PA" }, "UFPB": { "state": "PB" }, "UFPE": { "state": "PE" }, "IFMG-BAMBUI": { "state": "MG" }, "UFAC": { "state": "AC" }, "UFAL": { "state": "AL" }, "UFAM": { "state": "AM" }, "UFBA": { "state": "BA" }, "UFC": { "state": "CE" }, "UNB": { "state": "DF" }, "UNB-GAMA": { "state": "DF" }, "UNICAMP": { "state": "SP" }, "UNICAP": { "state": "PE" }, "UNIOESTE": { "state": "PR" }, "IFMG-OUROBRANCO": { "state": "MG" }, "IFMS-GERAL": { "state": "MS" }, "IFNMG-GERAL": { "state": "MG" }, "IFSP-GERAL": { "state": "SP" }, "IFSUL-GERAL": { "state": "RS" }, "IFSULMINAS-GERAL": { "state": "MG" }, "IFAM-GERAL": { "state": "AM" }, "ANTORO": { "state": "CE" }, "CEFETMG": { "state": "MG" }, "CESAR": { "state": "PE" }, "USP": { "state": "SP" }, "USP-SC": { "state": "SP" }, "UTFPR": { "state": "PR" }, "INATEL": { "state": "MG" }, "INTELI": { "state": "SP" }, "ITA": { "state": "SP" }, "PUC-GOIAS": { "state": "GO" }, "PUCMINAS": { "state": "MG" }, "PUCPR": { "state": "PR" }, "PUCRIO": { "state": "RJ" }, "UNIPAMPA": { "state": "RS" }, "UNIR": { "state": "RO" }, "UNIRIO": { "state": "RJ" }, "UNISAL": { "state": "SP" }, "UNIVILLE": { "state": "SC" }, "UNOESC": { "state": "SC" }, "UNOESTE": { "state": "SP" }, "UFSC": { "state": "SC" }, "UFFS": { "state": "SC" }, "UNIFESP": { "state": "SP" }, "CIMATEC": { "state": "BA" }, "FGV": { "state": "RJ" }, "IFAC-GERAL": { "state": "AC" }, "IFBA-IRECE": { "state": "BA" }, "IFBA-JACOBINA": { "state": "BA" }, "IFB-GERAL": { "state": "DF" }, "IFCE-GERAL": { "state": "CE" }, "IFPA-GERAL": { "state": "PA" }, "IFPB-ESPERANCA": { "state": "PB" }, "IFPE-CARUARU": { "state": "PE" }, "IFPI-GERAL": { "state": "PI" }, "IFPR-GERAL": { "state": "PR" }, "IFRJ-GERAL": { "state": "RJ" }, "IFRN-GERAL": { "state": "RN" }, "IFTM-GERAL": { "state": "MG" }, "PUCSP": { "state": "SP" }, "UDESC": { "state": "SC" }, "UFMT": { "state": "MT" }, "UFOPA": { "state": "PA" }, "UFPI": { "state": "PI" }, "UFPR": { "state": "PR" }, "UEMS": { "state": "MS" }, "UEPA": { "state": "PA" }, "UERJ": { "state": "RJ" }, "UERN": { "state": "RN" }, "UESC": { "state": "BA" }, "UESPI": { "state": "PI" }, "UFABC": { "state": "SP" }, "UEFS": { "state": "BA" }, "UECE": { "state": "CE" }, "UEM": { "state": "PR" }, "UFAPE": { "state": "PE" }, "UFCG": { "state": "PB" }, "IME": { "state": "RJ" }, "UFES": { "state": "ES" }, "UFSCAR": { "state": "SP" }, "UFV": { "state": "MG" }, "UNIBALSAS": { "state": "MA" }, "CFB": { "state": "CE" }, "UNISINOS": { "state": "RS" }, "UNIVASF": { "state": "BA" }, "UNILASALLE-RJ": { "state": "RJ" }, "UNESP": { "state": "SP" }, "UPE": { "state": "PE" }, "UPF": { "state": "RS" }, "SENAC-MS": { "state": "MS" }, "SENAI": { "state": "SP" }, "SESI-AF": { "state": "PR", "correct": "SESI-PR" }, "COTUCA": { "state": "SP" }, "ESAMC": { "state": "MG" }, "ETEC-HORTO": { "state": "SP" }, "UNINASSAU": { "state": "PI", "secondState": "PE" }, "FATEC": { "state": "SP" }, "FIAP": { "state": "SP" }, "FMM": { "state": "AM" }, "UNINOVE": { "state": "SP" }, "IESB": { "state": "DF" }, "PUCC": { "state": "SP" }, "UEA": { "state": "AM" }, "UFCA": { "state": "CE" }, "UFG": { "state": "GO" }, "UFGD": { "state": "MS" }, "UFJF": { "state": "MG" }, "UFPEL": { "state": "RS" }, "UFSJ": { "state": "MG" }, "UNEMAT": { "state": "MT" }, "UNIFEI": { "state": "MG" }, "UNIFESSPA": { "state": "PA" }, "UNIMONTES": { "state": "MG" }, "UNISENAISC": { "state": "PR", "correct": "UNISENAI-PR" }, "FAMETRO": { "state": "AM" }, "FECAP": { "state": "SP" }, "FEI": { "state": "SP" }, "UFSM": { "state": "RS" }, "UFU": { "state": "MG" }, "UNIFIP": { "state": "PB" }, "UNIJUI": { "state": "RS" }, "UPM": { "state": "SP" }, "CM": { "state": "PB" }, "IFSMG-RIOPOMBA": { "state": "RS", "correct": "IFRGS" }, "FHO-UNIARARAS": { "state": "SP" }, "MANUELABDI": { "state": "CE" }, "ROSASC": { "state": "SP" }, "UFS-BR": { "state": "SE" } }));

  // para o timer de 60 segundos que recarrega a página
  const realSetTimeout = window.setTimeout;
  window.setTimeout = function(fn, ...args) {
    if (fn.toString().match('window.location.reload')) {
      console.log('[INFO] Reloads every 60 seconds stopped');
      return;
    }
    realSetTimeout(fn, ...args);
  };

  // espera página carregar
  await new Promise((resolve, reject) => {
    document.addEventListener("DOMContentLoaded", (event) => { resolve(); });
  });

  function addScript(src) {
    return new Promise((resolve, reject) => {
      const el = document.createElement('script');
      el.src = src;
      el.addEventListener('load', resolve);
      el.addEventListener('error', reject);
      document.body.append(el);
    })
  };

  // importa Vue
  await addScript("https://unpkg.com/vue@3");

  // experimentos com live
  // const socket = io.connect('https://live.beecrowd.com:8080/', { 'transports': ['websocket', 'polling'] });
  // socket.on('live-contest', function({ id, challenge: { position }, answer: { id: answerId, answer }, timestamp, user: { id: userId, username } }) {
  //   console.log(id, position, answerId, answer, timestamp, userId, username);
  // });

  // ajusta elemento principal de conteúdo onde vai ser montado o Vue
  const contentElement = document.querySelector('.content.page');
  contentElement.classList.remove('page');
  contentElement.id = 'content';
  contentElement.style = 'width: 1485px; display: flex; gap: 16px';

  // armazena o script e o HTML do temporizador para restaurar depois
  const sidebarCountdownHtml = contentElement.querySelector('#side-bar #countdown').innerHTML;
  const sidebarScript = contentElement.querySelector('#side-bar script')?.innerHTML ?? "";

  const liElement = document.createElement('li');
  const aLiElement = document.createElement('a');
  const taskMode = Vue.ref(true);
  aLiElement.textContent = '+Placar';
  aLiElement.href = "";
  aLiElement.addEventListener('click', (e) => {
    e.preventDefault();
    taskMode.value = !taskMode.value;
    aLiElement.textContent = taskMode.value ? '+Placar' : '+Tarefas';
  });
  liElement.appendChild(aLiElement);
  document.body.querySelector('#menu').appendChild(liElement);

  // template do Vue
  document.body.querySelector('#content').innerHTML = `
    <div class="page" style="width: 990px">
      <div id="side-bar">
        <div id="countdown" class="mini" v-html="sidebarCountdownHtml"></div>
      </div>
      <div class="main-content" style="width: 730px">
        <div id="page-name" class="pn-rank">
          <h1 style="display: flex; align-items: center; justify-content: space-between">RANK<div v-if="loading" class="loading" style="width: 50px"></div></h1>
          <p>Fase Zero 2024</p>
        </div>
        <div style="display: flex; height: 22px; justify-content: space-between">
          <ul id="menu" style="display: flex; margin: 0">
            <li v-for="(filter, ix) in filters">
              <h4 v-if="selectedFilter == ix" class="c-mode-online" id="contest-mode">{{ filter.name }}</h4>
              <a href @click.prevent="selectedFilter = ix" v-else>{{ filter.name }}</a>
            </li>
          </ul>
          <input placeholder="🔍" v-model="searchText">
        </div>
      </div>
      <div class="main-content-big">
        <div id="table"><div class="list"><div id="element">
          <table id="contest-rank">
            <thead>
              <th>🌎</th>
              <th><a href @click.prevent="starMode = !starMode">{{ starMode ? '⭐' : '🔢' }}</a></th>
              <th class="left">Time</th>
              <th v-for="problem in problems">
                <a :href="problem.href" target="_blank">{{ problem.key }}</a>
              </th>
              <th>Total</th>
            </thead>
            <tbody>
              <tr v-for="score in filteredScoreboard" :key="score.nameId" style="background: white">
                <td class="c-box-h">{{ score.pos }}</td>
                <td class="c-box-h c-fav">
                  <a @click="favorites.has(score.nameId) ? favorites.delete(score.nameId) : favorites.add(score.nameId)">⭐</a>
                  {{ score.posFiltered }}
                </td>
                <td class="c-contestant">
                  <a :href="score.nameHref" target="_blank">{{ score.name }}</a>
                  <br>
                  <em>{{ score.institution }}</em>
                </td>
                <td class="c-box" v-for="problem in problems">
                  <div :class="score.problems[problem.key] && score.problems[problem.key][0] === 'YESF' ? 'first-to-solve' : 'c-status'">
                      <p v-if="score.problems[problem.key] && score.problems[problem.key][0].startsWith('YES')" class="c-yes" :class="\`c-yes-\${problem.key.toLowerCase()}\`">{{ score.problems[problem.key][1] }}</p>
                      <p v-else-if="score.problems[problem.key]" class="c-no">{{ score.problems[problem.key][1] }}</p>
                      <p v-else> </p>
                      <span v-if="score.problems[problem.key]">{{ score.problems[problem.key][2] }}</span>
                      <span v-else>•</span>
                  </div>
                </td>
                <td class="c-box-t">
                  <div class="c-status">
                    <p>{{ score.total[0] }}</p>
                    <span>{{ score.total[1] }}</span>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
          <p id="academic-footer">© 2024 beecrowd Contests
            <a href="www.beecrowd.com" target="_blank">www.beecrowd.com</a>
          </p>
        </div></div></div>
      </div>
    </div>
    <div class="page" style="width: 478px;">
      <table class="task-list">
        <thead>
          <th>Tempo</th>
          <th>Nome</th>
          <th>Instituição</th>
          <th>Desafio</th>
          <th v-if="taskMode">Ação</th>
        </thead>
        <tbody is="vue:transition-group" name="list">
         <tr v-for="[taskId, [time, nameId, name, institution, problem, first], info] in taskListReversedFiltered" :class="[taskMode && info, first && 'first-to-solve']" :key="taskId">
           <td class="right">{{time}}</td>
           <td>{{name}}</td>
           <td class="center">{{institution.split(', ').slice(0, -1).join(', ')}}</td>
           <td class="c-yes center" :class="\`c-yes-\${problem.toLocaleLowerCase()}\`">{{ problem }}</td>
           <td v-if="taskMode">
             <button v-if="info == 'open'" @click="setTask(taskId, 'closed')">FECHAR</button>
             <button v-else @click="setTask(taskId, 'open')">ABRIR</button>
           </td>
         </tr>
        </tr>
      </table>
    </div>
`;

  // estilos
  const styleElement = document.createElement('style');
  styleElement.innerHTML = `
.list-move, /* apply transition to moving elements */
.list-enter-active,
.list-leave-active {
  transition: all 0.5s ease;
}

.list-enter-from,
.list-leave-to {
  opacity: 0;
  transform: translateX(30px);
}

/* ensure leaving items are taken out of layout flow so that moving
   animations can be calculated correctly. */
.list-leave-active {
  position: absolute;
}

.fade-enter-active,
.fade-leave-active {
  transition: opacity 0.5s ease;
}

.fade-enter-from,
.fade-leave-to {
  opacity: 0;
}

.main-content {
    height: 155px;
}
.blink {
    animation: changeColor ease;
    animation-iteration-count: infinite;
    animation-duration: 0.5s;
    animation-fill-mode: both;
}
@keyframes changeColor
{
    0%   {background-color: #ffffff;}
    50%  {background-color: #f3f3f3;}
    100% {background-color: #ffffff;}
}
.task-list td {
    padding: 12px;
}
.task-list .open {
  background: #fbb;
}
.task-list .closed {
  background: #bfb;
}
.task-list button {
  background: #f3f3f3;
  border: #bbb solid 1px;
  border-radius: 3px;
  display: inline-block;
  font-size: 10px;
  margin: 0 2px;
  padding: 5px 7px;
  color: #454545;
  outline: 0;
  text-decoration: none;
  cursor: pointer;
  width: 54px;
}
.task-list button:hover {
  background: #662483;
  border-color: #662483;
  color: #fff;
}
.c-fav { position: relative; }
.c-fav a {
    opacity: 0;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    cursor: pointer;
    padding: 10px;
}
.c-fav a:hover {
    opacity: 1;
}`;
  document.body.appendChild(styleElement);

  Vue.productionTip = true;
  Vue.createApp({
    setup() {
      const problems = Vue.ref([]);
      const scoreboard = Vue.ref([]);
      const loading = Vue.ref(false);
      const searchText = Vue.ref("");
      const parser = new DOMParser();
      async function updateScoreboard() {
        loading.value = true;
        const scoreboardPageResponse = await fetch(document.location.href);
        const scoreboardPageHtml = await scoreboardPageResponse.text();
        const scoreboardPage = parser.parseFromString(scoreboardPageHtml, "text/html");
        problems.value = [...scoreboardPage.querySelectorAll('#contest-rank thead th')].slice(2, -1).map(th => {
          const a = th.querySelector('a') ?? th;
          return { key: a.textContent, href: a.href };
        });
        function processLine(tr) {
          const tds = tr.querySelectorAll('td');
          const [posTd, nameTd, ...problemsAndTotalTds] = tds;
          const problemsTds = problemsAndTotalTds.slice(0, -1);
          const totalTd = problemsAndTotalTds[problemsAndTotalTds.length-1];
          const pos = +posTd.textContent;
          const nameLink = nameTd.querySelector('a');
          const nameId = +nameLink.href.split('/').slice(-1)[0];
          const name = nameLink.textContent;
          let fullInstitution = nameTd.querySelector('em').textContent.trim().split(', ');
          const [institution, country] = fullInstitution;
          if (country == 'Brazil' && institutionInfo.has(institution)) {
            const info = institutionInfo.get(institution);
            fullInstitution = `${info.correct ?? institution}, ${stateToRegion.get(info.state)}, Brasil`;
          } else {
            fullInstitution = [institution, country].join(', ');
          }
          const problems = Object.fromEntries(problemsTds.filter(problemTd => problemTd.querySelector('p')).map((problemTd, i) => {
            const key = String.fromCharCode('A'.charCodeAt(0) + i);
            const p = problemTd.querySelector('p');
            if (p.classList.length === 0) { return [key, null]; }
            const submissionCount = +p.textContent;
            const span = problemTd.querySelector('span');
            const time = +span.textContent;
            if (p.parentNode.classList.contains('first-to-solve')) {
              return [key, ['YESF', submissionCount, time]];
            }
            if (p.classList.contains('c-yes')) {
              return [key, ['YES', submissionCount, time]];
            }
            return [key, ['NO', submissionCount, time]];
          }));
          const total = [+totalTd.querySelector('p').textContent, +totalTd.querySelector('span').textContent];
          return { pos, name, nameId, nameHref: nameLink.href, institution: fullInstitution, problems, total, tr };
        }
        scoreboard.value = [...scoreboardPage.querySelectorAll('tr')].slice(1, -3).filter(tr => tr.querySelector('a')).map(tr => processLine(tr));
        loading.value = false;
      }

      Vue.onMounted(() => {
        // é
        eval(sidebarScript);
        updateScoreboard();
        setInterval(() => updateScoreboard(), 15000);
      });

      const favorites = Vue.reactive(new Set(JSON.parse(localStorage.getItem('favorites') ?? '[]')));
      Vue.watchEffect(() => {
         localStorage.setItem('favorites', JSON.stringify([...favorites]));
      });

      const filters = [
          { name: 'Tudo', match () { return true; } },
          { name: 'Sede ⭐', match (score) { return favorites.has(score.nameId); } },
          { name: 'Brasil', match (score) { return score.institution.toString().endsWith(', Brasil'); } },
          { name: 'Norte', match (score) { return score.institution.toString().endsWith(', Norte, Brasil'); } },
          { name: 'Nordeste', match (score) { return score.institution.toString().endsWith(', Nordeste, Brasil'); } },
          { name: 'Centro-Oeste', match (score) { return score.institution.toString().endsWith(', Centro-Oeste, Brasil'); } },
          { name: 'Sul', match (score) { return score.institution.toString().endsWith(', Sul, Brasil'); } },
          { name: 'Sudeste', match (score) { return score.institution.toString().endsWith(', Sudeste, Brasil'); } },
      ];
      const selectedFilter = Vue.ref(0);
      const starMode = Vue.ref(true);
      const filteredScoreboard = Vue.computed(() => {
        let i = 1
        return scoreboard.value.filter(score => filters[selectedFilter.value].match(score)
                                       && (score.name.toLocaleLowerCase().includes(searchText.value.toLocaleLowerCase()) || score.institution.toString().toLocaleLowerCase().includes(searchText.value.toLocaleLowerCase()))).map(score => ({
          ...score,
          posFiltered: (starMode.value ? favorites.has(score.nameId) : true) ? i++ : null
        }));
      });

      const taskList = Vue.reactive(JSON.parse(localStorage.getItem('taskList') ?? '[]'));
      Vue.watchEffect(() => {
        const derivedTaskList = (taskMode.value ? filteredScoreboard : scoreboard).value.flatMap(score => Object.entries(score.problems).filter(([, v]) => v && v[0].startsWith('YES'))
          .map(([k, v]) => [v[2], score.nameId, score.name, score.institution, k, v[0] === 'YESF']));
        const existingTasksJson = new Set(taskList.map(([_taskId, task, _info]) => JSON.stringify(task)));
        const newTasks = derivedTaskList.filter(task => !existingTasksJson.has(JSON.stringify(task))).sort((a, b) => a[0] - b[0]);
        newTasks.forEach(task => {
          taskList.push([taskList.length, task, 'open']);
        });
        saveTaskList();
      }, [scoreboard, filteredScoreboard]);
      function saveTaskList() {
        localStorage.setItem('taskList', JSON.stringify(taskList.map(x => [x[0], x[1], x[2]])));
      }
      const taskListReversedFiltered = Vue.computed(() => {
        const reversed = taskList.filter(x => !taskMode.value || favorites.has(x[1][1])).reverse();
        if (!taskMode.value) { return reversed.slice(0, 20); }
        return reversed;
      });

      function setTask(taskId, info) {
        taskList[taskId] = [taskId, taskList[taskId][1], info];
      }
      return { sidebarCountdownHtml, problems, scoreboard, filteredScoreboard, taskList, taskListReversedFiltered, loading, favorites, taskMode, setTask, filters, selectedFilter, starMode, searchText };
    }
  }).mount('#content');
})();
